/**
*Crear Una Base de Datos
*/
CREATE DATABASE Loans  
ON   
( NAME = Loans,  
    FILENAME = 'Loans.mdf',  
    SIZE = 10,  
    MAXSIZE = 50,  
    FILEGROWTH = 5 )  

/**
*Borrar una base de datos
*/

DROP DATABASE Loans;

/**
*Crear Una Tabla
*/
CREATE TABLE Loans(id int IDENTITY(1,1) NOT NULL,monto float NOT NULL);

/**
*Crear Tabla
*/
Create Table Prestamos (comlumna1 int, columna2 varchar(255));

/**
*Modificar Tabla
*/
ALTER TABLE Prestamos ALTER COLUMN comlumna1 char(89) NOT NULL;

/**
*Eliminar una Tabla
*/
DROP TABLE Prestamos;

/**
*Truncar una Tabla
*/
TRUNCATE TABLE Prestamos;
