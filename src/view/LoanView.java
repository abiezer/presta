package view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;


public class LoanView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField numberField;
	private JTextField personField;
	private JTextField amountField;
	private JComboBox<Object> quotaComboBox;
	private JButton btnRegister;
	private JButton btnModify;
	private JButton btnDelete;
	private JButton btnSearch;
	
	
	/**
	 * Create the frame.
	 */
	public LoanView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 414, 239);
		contentPane.add(panel);
		panel.setLayout(null);
		
		btnRegister = new JButton("Registrar");
		btnRegister.setBounds(315, 11, 89, 23);
		panel.add(btnRegister);
				
		btnModify = new JButton("Modificar");
		btnModify.setBounds(315, 45, 89, 23);
		panel.add(btnModify);
		
		btnDelete = new JButton("Eliminar");
		btnDelete.setBounds(315, 79, 89, 23);
		panel.add(btnDelete);
		
		btnSearch = new JButton("Buscar");
		btnSearch.setBounds(315, 113, 89, 23);
		panel.add(btnSearch);
		
		numberField = new JTextField();
		numberField.setBounds(82, 12, 195, 20);
		panel.add(numberField);
		numberField.setColumns(10);
		
		personField = new JTextField();
		personField.setBounds(82, 46, 195, 20);
		panel.add(personField);
		personField.setColumns(10);
		
		
		amountField = new JTextField();
		amountField.setBounds(82, 80, 195, 20);
		panel.add(amountField);
		amountField.setColumns(10);
		
		quotaComboBox = new JComboBox<Object>();
		quotaComboBox.setModel(new DefaultComboBoxModel<Object>(new String[] {"6", "12", "24", "36", "48"}));
		quotaComboBox.setSelectedIndex(0);
		quotaComboBox.setBounds(82, 114, 195, 20);
		panel.add(quotaComboBox);		
		
		JLabel lblNumero = new JLabel("Numero");
		lblNumero.setBounds(10, 15, 46, 14);
		panel.add(lblNumero);
		
		JLabel lblPersona = new JLabel("Persona");
		lblPersona.setBounds(10, 49, 46, 14);
		panel.add(lblPersona);
		
		JLabel lblPersona_1 = new JLabel("Monto");
		lblPersona_1.setBounds(10, 83, 46, 14);
		panel.add(lblPersona_1);
		
		JLabel lblCuotas = new JLabel("Cuotas");
		lblCuotas.setBounds(10, 117, 46, 14);
		panel.add(lblCuotas);
		
		
	}
	

	
	public JComboBox<Object> getQuotaComboBox() {
		return quotaComboBox;
	}



	public void setQuotaComboBox(JComboBox<Object> quotaComboBox) {
		this.quotaComboBox = quotaComboBox;
	}



	public JButton getBtnModify() {
		return btnModify;
	}

	public void setBtnModify(JButton btnModify) {
		this.btnModify = btnModify;
	}

	public JButton getBtnDelete() {
		return btnDelete;
	}

	public void setBtnDelete(JButton btnDelete) {
		this.btnDelete = btnDelete;
	}

	public JButton getBtnSearch() {
		return btnSearch;
	}

	public void setBtnSearch(JButton btnSearch) {
		this.btnSearch = btnSearch;
	}

	public JButton getBtnRegister() {
		return btnRegister;
	}

	public void setBtnRegister(JButton btnRegister) {
		this.btnRegister = btnRegister;
	}


	public JTextField getNumberField() {
		return numberField;
	}


	public void setNumberField(JTextField numberField) {
		this.numberField = numberField;
	}


	public JTextField getPersonField() {
		return personField;
	}


	public void setPersonField(JTextField personField) {
		this.personField = personField;
	}


	public JTextField getAmountField() {
		return amountField;
	}


	public void setAmountField(JTextField amountField) {
		this.amountField = amountField;
	}
}
