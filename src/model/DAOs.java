package model;

import java.util.ArrayList;

public interface DAOs<T> {
	public abstract boolean register();
	public abstract boolean update();
	public abstract boolean delete();
	public abstract T search();
	public abstract ArrayList<LoanModel> showAll();
	
}

