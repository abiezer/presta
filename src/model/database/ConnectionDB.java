package model.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

public class ConnectionDB {
	// Create a variable for the connection string.
	private final String connectionUrl = "jdbc:sqlserver://sql_cap_aws.crecic.cl:1433;databaseName=dbo.Loans;user=sa;password=crecic2018";
	
	public Statement getConnectionDB() {
		Connection con;
		try {
			con = DriverManager.getConnection(this.connectionUrl);
			return con.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,e.getMessage(),"Error a retornar la Conexion",  JOptionPane.ERROR_MESSAGE);
		}
		
		return null;
		
	}
	
	public void consulta(String consulta) {

        

        try (Connection con = DriverManager.getConnection(this.connectionUrl); Statement stmt = con.createStatement();) {
            String SQL = consulta;
            //CREATE TABLE Table Loans( id int NOT NULL identity, descripcion varchar(255))
            ResultSet rs = stmt.executeQuery(SQL);

             //Iterate through the data in the result set and display it.
            while (rs.next()){
                System.out.println(rs.getString("palabra") + "  |  " + rs.getString("significado"));
            }
        }
        // Handle any errors that may have occurred.
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
