package model;

public class LoanModel {
	
	private String number;
	private String person;
	private double amount;
	private int quota;
	
	
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getPerson() {
		return person;
	}
	public void setPerson(String person) {
		this.person = person;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public int getQuota() {
		return quota;
	}
	public void setQuota(int cuotas) {
		this.quota = cuotas;
	}
	
	
	
}
